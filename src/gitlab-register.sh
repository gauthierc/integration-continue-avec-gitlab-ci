gitlab-runner register \
  --non-interactive \
  --url "<URL SERVEUR GITLAB>" \
  --registration-token "PROJECT_REGISTRATION_TOKEN" \
  --executor "<TYPE>" \
  --docker-image <IMAGE DOCKER PAR DEFAUT> \
  --description "<DESCRIPTION>" \
  --tag-list "<TAG1, TAG2...>" \
  --run-untagged \
  --locked="false"
